# Diaporamas

## Ces diaporamas sont produits à partir du  Framework [ReavealJS](https://revealjs.com/) et déployés dans la ForgeÉdu avec la fontionnalité pages


Forge et Mardown:  
https://drane-grenoble.forge.apps.education.fr/diaporamas/forge/  

Moodle Éléa:
https://drane-grenoble.forge.apps.education.fr/diaporamas/elea/  

IA:  
https://drane-grenoble.forge.apps.education.fr/diaporamas/IA/  

Capytale:  
https://drane-grenoble.forge.apps.education.fr/diaporamas/capytale/  

Ressources, logiciels et Sytèmes d'exploitation libres:  
https://drane-grenoble.forge.apps.education.fr/diaporamas/libres/
